# depreciated

This is a gtk theme I'm making, it's generated using [Oomox](https://github.com/themix-project/oomox) and it's based on [Materia](https://github.com/nana-4/materia-theme).

I pair this with the dwm build I use found in my [dotfiles](https://gitlab.com/10leej/dotfiles) (.dwm/dwm/)
